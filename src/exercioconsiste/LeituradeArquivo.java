package exercioconsiste;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LeituradeArquivo {

	public static void main(String[] args) {

		List<Pessoa> listaPessoa = new ArrayList<>();

		String path = "input\\dataset.csv";

		try(BufferedReader br = new BufferedReader(new FileReader(path))) {
			br.readLine();
			String linha = br.readLine();
			while(linha != null && linha != "") {
				String[] linhaQuebrada = linha.split(";");
				Integer tamanhoArray = linhaQuebrada.length; 

				String primeiroNome = linhaQuebrada[0].toUpperCase().trim().replaceAll("\\s+", " ");
				String sobrenomes = linhaQuebrada[1].toUpperCase().trim().replaceAll("\\s+", " ");

				Double pesoCorrigido = null;
				Double alturaCorrigida = null;
				if(tamanhoArray == 4) {
					if(linhaQuebrada[2] != null) {
						pesoCorrigido = Double.parseDouble(linhaQuebrada[2].replace(',' , '.'));	
					}
					if(linhaQuebrada[3] != null) {
						alturaCorrigida = Double.parseDouble(linhaQuebrada[3].replace(',' , '.'));	
					}
				}

				Pessoa pessoa = new Pessoa(primeiroNome, sobrenomes, pesoCorrigido, alturaCorrigida);
				pessoa.calcularIMC();
				listaPessoa.add(pessoa);
				linha = br.readLine();
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		String pathOut = "output\\anaVeronicaSilvaDeAraujo.txt";

		try(BufferedWriter bw = new BufferedWriter(new FileWriter(pathOut))){

			for (Pessoa pessoa : listaPessoa) {
				String imc = "Dados incompletos, impossível calcular";
				if(pessoa.getImc() != null) {
					DecimalFormat df = new DecimalFormat("0.00");
					imc = df.format(pessoa.getImc());
				}
				bw.write(pessoa.getPrimeiroNome() + " " +  pessoa.getSobrenomes() + " " + imc);
				bw.newLine();
				System.out.print(pessoa.getPrimeiroNome() + " " +  pessoa.getSobrenomes() + " ");
				System.out.printf(imc);
				System.out.println();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}