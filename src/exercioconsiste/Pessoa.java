package exercioconsiste;

public class Pessoa {

	private String primeiroNome;
	private String sobrenomes;
	private Double peso;
	private Double altura;
	private Double imc;

	public Pessoa(String primeiroNome, String sobrenomes, Double peso, Double altura) {
		this.primeiroNome = primeiroNome;
		this.sobrenomes = sobrenomes;
		this.peso = peso;
		this.altura = altura;
	}

	public void calcularIMC() {
		if (altura == null || peso == null) {
			imc = null;
		}
		else {
			imc = peso / (altura * altura);
		}
	}

	public String getPrimeiroNome() {
		return primeiroNome;
	}

	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	public String getSobrenomes() {
		return sobrenomes;
	}

	public void setSobrenomes(String sobrenomes) {
		this.sobrenomes = sobrenomes;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public Double getImc() {
		return imc;
	}

	public void setImc(Double imc) {
		this.imc = imc;
	}
	
}